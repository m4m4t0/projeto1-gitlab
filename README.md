# Projeto1 gitlab

Este programa possui tres funções: randArray1(), randArray10() e randArray100(), as quais retornam doule floats aleatórios de 0 a 1, 0 a 10 e 0 a 100. Essas recebem um int o qual define o tamanho do array que elas retornarão, cada uma delas possuí o seu próprio arquivo fonte e header, para isso o arquivo Makefile inclui as medidas necessárias para compilação.

Na função "main.c", é pedido a definição de 3 valores de int os quais serão os tamanhos das funções citadas acima nessa ordem. Feito isso os arrays retornados pelas funções são printados no terminal.
